package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.repository.model.ITaskGraphRepository;
import ru.vpavlova.tm.api.repository.model.IUserGraphRepository;
import ru.vpavlova.tm.api.service.model.ITaskGraphService;
import ru.vpavlova.tm.entity.TaskGraph;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyDescriptionException;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;

import java.util.List;
import java.util.Optional;

@Service
public final class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskGraphService {

    @NotNull
    @Autowired
    public ITaskGraphRepository taskGraphRepository;

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    public ITaskGraphRepository getRepository() {
        return taskGraphRepository;
    }

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return userGraphRepository;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final TaskGraph task = new TaskGraph();
        task.setName(name);
        task.setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        task.setUser(userRepository.findById(userId).get());
        taskRepository.add(task);
        return task;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable TaskGraph entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final TaskGraph task) {
        if (task == null) throw new ObjectNotFoundException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.add(task);
    }

    @Override
    @Transactional
    public void addAll(@NotNull List<TaskGraph> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        entities.forEach(taskRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.clear();
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public List<TaskGraph> findAll() {
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findAll();
    }

    @Override
    @SneakyThrows
    public void removeById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeById(id);
    }

    @NotNull
    @Override
    public Optional<TaskGraph> findById(
            @Nullable String id
    ) {
        if (id == null) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.clearByUserId(userId);
    }

    @NotNull
    @Override
    @Transactional
    public List<TaskGraph> findAll(@Nullable String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<TaskGraph> findOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<TaskGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<TaskGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        return taskRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final TaskGraph entity
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneByIdAndUserId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        @NotNull Optional<TaskGraph> task = taskRepository.findOneByIndex(userId, index);
        taskRepository.removeOneByIdAndUserId(userId, task.get().getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.removeOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<TaskGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<TaskGraph> entity = findOneById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<TaskGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final ITaskGraphRepository taskRepository = getRepository();
        taskRepository.update(entity.get());
    }

}
