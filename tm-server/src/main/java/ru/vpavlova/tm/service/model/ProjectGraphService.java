package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.repository.model.IProjectGraphRepository;
import ru.vpavlova.tm.api.repository.model.IUserGraphRepository;
import ru.vpavlova.tm.api.service.model.IProjectGraphService;
import ru.vpavlova.tm.entity.ProjectGraph;
import ru.vpavlova.tm.enumerated.Status;
import ru.vpavlova.tm.exception.empty.EmptyDescriptionException;
import ru.vpavlova.tm.exception.empty.EmptyIdException;
import ru.vpavlova.tm.exception.empty.EmptyNameException;
import ru.vpavlova.tm.exception.empty.EmptyUserIdException;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.system.IndexIncorrectException;

import java.util.List;
import java.util.Optional;

@Service
public final class ProjectGraphService extends AbstractGraphService<ProjectGraph> implements IProjectGraphService {

    @NotNull
    @Autowired
    public IProjectGraphRepository projectGraphRepository;

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    public IProjectGraphRepository getRepository() {
        return projectGraphRepository;
    }

    @NotNull
    public IUserGraphRepository getUserRepository() {
        return userGraphRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.add(entity);
    }

    @Override
    @SneakyThrows
    public ProjectGraph add(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId != null) throw new EmptyUserIdException();
        if (name != null) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectGraph project = new ProjectGraph();
        project.setName(name);
        project.setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        @NotNull final IUserGraphRepository userRepository = getUserRepository();
        project.setUser(userRepository.findById(userId).get());
        projectRepository.begin();
        projectRepository.add(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void addAll(@Nullable List<ProjectGraph> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        entities.forEach(projectRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.clear();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final ProjectGraph entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeById(entity.getId());
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll() {
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.clearByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectGraph> findAll(@Nullable final String userId) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findAllByUserId(userId);
    }


    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findOneByIdAndUserId(userId, id);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findOneByIndex(userId, index);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<ProjectGraph> findOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        return projectRepository.findOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(
            @Nullable final String userId, @Nullable final ProjectGraph entity
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneByIdAndUserId(userId, id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        @NotNull Optional<ProjectGraph> project = projectRepository.findOneByIndex(userId, index);
        projectRepository.removeOneByIdAndUserId(userId, project.get().getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeOneByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.removeOneByName(userId, name);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<ProjectGraph> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void changeStatusByName(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final Status status
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        if (status == null) throw new ObjectNotFoundException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(status);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<ProjectGraph> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void finishByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.COMPLETE);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startById(
            @Nullable final String userId, @Nullable final String id
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final Optional<ProjectGraph> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByIndex(
            @Nullable final String userId, @Nullable final Integer index
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void startByName(
            @Nullable final String userId, @Nullable final String name
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (name.isEmpty()) return;
        @NotNull final Optional<ProjectGraph> entity = findOneByName(userId, name);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setStatus(Status.IN_PROGRESS);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (id.isEmpty()) throw new EmptyIdException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<ProjectGraph> entity = findById(userId, id);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (index < 0) throw new IndexIncorrectException();
        if (name.isEmpty()) throw new EmptyNameException();
        @NotNull final Optional<ProjectGraph> entity = findOneByIndex(userId, index);
        if (!entity.isPresent()) throw new ObjectNotFoundException();
        entity.get().setName(name);
        entity.get().setDescription(description);
        @NotNull final IProjectGraphRepository projectRepository = getRepository();
        projectRepository.update(entity.get());
    }

}