package ru.vpavlova.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.vpavlova.tm.api.IPropertyService;
import ru.vpavlova.tm.api.repository.model.IUserGraphRepository;
import ru.vpavlova.tm.api.service.model.IUserGraphService;
import ru.vpavlova.tm.entity.UserGraph;
import ru.vpavlova.tm.enumerated.Role;
import ru.vpavlova.tm.exception.empty.*;
import ru.vpavlova.tm.exception.entity.ObjectNotFoundException;
import ru.vpavlova.tm.exception.entity.UserNotFoundException;
import ru.vpavlova.tm.util.HashUtil;

import java.util.List;
import java.util.Optional;

@Service
public final class UserGraphService extends AbstractGraphService<UserGraph> implements IUserGraphService {

    @NotNull
    @Autowired
    public IUserGraphRepository userGraphRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    public IUserGraphRepository getRepository() {
        return userGraphRepository;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable final UserGraph user) {
        if (user == null) throw new ObjectNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.add(user);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<UserGraph> findByLogin(
            @Nullable final String login
    ) {
        if (login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        return userRepository.findByLogin(login);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final UserGraph entity) {
        if (entity == null) throw new ObjectNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.removeById(entity.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.removeByLogin(login);
    }

    @Override
    @Transactional
    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(
            @Nullable final String userId, @Nullable final String password
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        if (password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final Optional<UserGraph> user = findById(userId);
        if (!user.isPresent()) return;
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) return;
        user.get().setPasswordHash(hash);
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.update(user.get());
    }

    @Override
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserGraph user = new UserGraph();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateUser(
            @Nullable final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (userId.isEmpty()) throw new EmptyUserIdException();
        @NotNull final Optional<UserGraph> user = findById(userId);
        if (!user.isPresent()) throw new ObjectNotFoundException();
        user.get().setFirstName(firstName);
        user.get().setLastName(lastName);
        user.get().setMiddleName(middleName);
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.update(user.get());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Optional<UserGraph> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final UserGraph user = userOptional.get();
        user.setLocked(true);
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.update(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @NotNull final Optional<UserGraph> userOptional = findByLogin(login);
        if (!userOptional.isPresent()) throw new UserNotFoundException();
        @NotNull final UserGraph user = userOptional.get();
        user.setLocked(false);
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.update(user);
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable List<UserGraph> entities) {
        if (entities == null) throw new ObjectNotFoundException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        entities.forEach(userRepository::add);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.clear();
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<UserGraph> findAll() {
        @NotNull final IUserGraphRepository userRepository = getRepository();
        return userRepository.findAll();
    }

    @NotNull
    @Override
    @SneakyThrows
    public Optional<UserGraph> findById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        return userRepository.findById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String id
    ) {
        if (id.isEmpty()) throw new EmptyIdException();
        @NotNull final IUserGraphRepository userRepository = getRepository();
        userRepository.removeById(id);
    }

}
